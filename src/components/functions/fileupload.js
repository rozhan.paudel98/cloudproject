
function upload(method, url, files, data) {
    return new Promise(function (resolve, reject) {
        const xhr = new XMLHttpRequest();
        const formData = new FormData();
        console.log("from upload js",files);
      
            
            formData.append('img', files);
     
        // deleting property of object
        // delete data.new_image;

        for (let item in data) {
            formData.append(item,data[item]);
            
        }
        xhr.onreadystatechange = () => {
            // logic behind xhr request

            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    resolve(xhr.response)
                } else {
                    reject(xhr.response);
                }
            }
        }
        xhr.open(method, url);
        xhr.send(formData);
        console.log("form data is ",formData);
    })


}
module.exports = {
    upload
}