import React, { Component } from 'react';
import {Link} from "react-router-dom";
import axios from "axios";
import { toast } from 'react-toastify';

export class LoginForm extends Component {
    constructor(props){
        super(props);
        this.state={
            email: null,
            password: null,
            isSubmitting: false,
            usernameError: null,
			passwordError : null,
			isLoggedIn: false,
        }
    }
    handleChange=(event)=>{

            this.setState({
                [event.target.name] : event.target.value
            })
            

    }
    handleSubmit=(e)=>{
        e.preventDefault();
        console.log(e);
        const {email,password}=this.state;
        let data ={
            username: email,
            password : password
		}
		const url="http://localhost:6500/users/login";
		//make http call 
		axios.post(url,data)
			.then((success)=>{
				console.log("Success is ", success);
				if(success.data.token && success.data.user)
				{
					toast.info("Login Success");
					localStorage.setItem("token",success.data.token);
				localStorage.setItem("user",success.data.user);
				if(localStorage.getItem("token")){
					this.props.history.push("/dashboard")
				}
				
				}
				else{
					toast.warn("Wrong Credentials")
				}
			
				
			})

			
	}
	

    render() {
        return (
            <div>

                <div className="limiter">
		<div className="container-login100">
			<div className="wrap-login100">
				<div className="login100-pic js-tilt" data-tilt>
					<img src="images/img-01.png" alt="IMG" />
				</div>

				<form className="login100-form validate-form">
					<span className="login100-form-title">
						Member Login
					</span>

					<div className="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input className="input100" type="text"  required={true} name="email"  onChange={this.handleChange} placeholder="Username"/>
						<span className="focus-input100"></span>
						<span className="symbol-input100">
							<i className="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div className="wrap-input100 validate-input" data-validate = "Password is required">
						<input className="input100" type="password" required={true} onChange={this.handleChange} name="password" placeholder="Password"/>
						<span className="focus-input100"></span>
						<span className="symbol-input100">
							<i className="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div className="container-login100-form-btn">
						<button onClick={this.handleSubmit} className="login100-form-btn">
							Login
						</button>
					</div>

					<div className="text-center p-t-12">
						<span className="txt1">
							Forgot
						</span>
						<Link className="txt2" to="/forgot-password">
							Username / Password?
						</Link>
					</div>

					<div className="text-center p-t-136">
						<Link className="txt2" to="/register">
							Create your Account
							<i className="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</Link>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	
            </div>
        )
    }
}
