import React, { Component } from 'react'
import "./register.component.css"
import axios from "axios";
import { toast } from 'react-toastify';
import {Link} from "react-router-dom";
const URL = "http://localhost:6500/users/register";


export  class RegiserForm extends Component {
    constructor(props){
        super(props);
        this.state={
            name: null,
            email: null,
            phonenumber: null,
            password: null,
            username:null,
            address:null,
            isSubmitting: false,
        }
    }
    handleChange=(event)=>{
        this.setState({
            [event.target.name]: event.target.value,
        })
    }
    handleSubmit=(e)=>{
    e.preventDefault(); 
    this.setState({ isSubmitting:true});
    setTimeout(()=>{
        this.setState({
            isSubmitting: false
        })
    },1100)
    const {name,username,password,email,address,phonenumber}= this.state;
    const data= {
        name,username,password,email,address,phonenumber
    };
   //sending data to server
    axios.post(URL,data)
        .then((success)=>{
            if(success.data._id){
                //show toast message here
                toast.success("Registration Completed");
                toast.info(" Please login to Continue....")
                //redirect the page
                this.props.history.push("/");
            }
        })
        .catch((error)=>{
            console.log("error is ",error);
        })
    

}




    render() {
        var btn = this.state.isSubmitting
        ?<button  disabled={true} type="submit" className="btn btn-primary">Submitting...</button> 
        :<button type="submit" className="btn btn-primary">Register</button>
        return (
            <div className="container">
                <br></br><br></br>
                <h1>Registration Form </h1>
                <br></br>
                <p>Please register to use free Cloud Services</p>

                <form className="form-group" onSubmit={this.handleSubmit} >
                    <label htmlFor="name">Full Name</label>
                    <input type="text" className="form-control" onChange={this.handleChange} placeholder="Full Name" name="name" id="name"/>
                    <label htmlFor="email">Email</label>
                    <input type="text" className="form-control" onChange={this.handleChange} placeholder="Email" name="email" id="email"/>
                    <label htmlFor="username">Username</label>
                    <input type="text" name="username" className="form-control" onChange={this.handleChange}id="username" placeholder="Username"></input>
                    <label htmlFor="password">Password</label>
                    <input type="password" className="form-control" onChange={this.handleChange} placeholder="Enter your password here" name="password" id="password"/>
                    <label htmlFor="phonenumber">PhoneNumber</label>
                    <input type="number" className="form-control" onChange={this.handleChange} placeholder="Contact Number" name="phonenumber" id="phonenumber"/>
                    <label htmlFor="address">Address</label>
                    <input type="text" className="form-control" onChange={this.handleChange} name="address" placeholder="Address"></input>
                    <br></br>
                    {btn} <Link  to="/" type="button"> Go Back </Link>

                </form>
                
            </div>
        )
    }
}
