import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./dashboard.component.css";
import { toast } from "react-toastify";
import "./crudComponents/showproduct.css";

export class Dashboard extends Component {
  constructor(props) {
    super(props);
  }

  logoutfromapp = () => {
    localStorage.clear();
    toast.info("Successfully Signout ");
    this.props.history.push("/");
  };

  render() {
    return (
      <div className="mainD">
        <ul className="sidebar">
          <hr></hr>
          <li className="insidelionefirst">
            <Link to="/dashboard">Dashboard</Link>
          </li>
          <hr></hr>
          <li className="insidelione">
            <Link to="/view-files">Products</Link>
          </li>
          <hr></hr>
          <li className="insidelione">
            <Link to="/add-item">Add item</Link>
          </li>
          <hr></hr>
          <li className="insidelione">
            <Link to="/user-setting">Change User Credentials</Link>
          </li>
          <hr></hr>
          <li>
            {" "}
            <button
              className="btn btn-danger"
              onClick={this.logoutfromapp}
              id="logout"
            >
              Log Out
            </button>
          </li>
        </ul>

        <div className="content">
          <h1>Dashboard</h1>

          {/* <br/><br/><br/> */}
          <p className="paradash">
            {" "}
            This dashboard is only <strong>available </strong> to the loggedin
            users{" "}
          </p>
          <p className="paradash">
            {" "}
            <strong>
              If you haven't loggedin and you are on this page then kindly
              ignore this page
            </strong>
          </p>
          <br></br>
          <br></br>
          <p className="paradash">
            Or{" "}
            <strong>
              Contact to system admin Roshan Paudel @ Phone-Number
            </strong>
          </p>
        </div>
      </div>
    );
  }
}
