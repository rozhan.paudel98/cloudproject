import React from 'react'
import "./navbar.css"

export default function TopNavbar() {
    return (
        <div>
            <nav class="nav mt-5">
  <a href="#" class="nav-item is-active" active-color="orange">Home</a>
  <a href="#" class="nav-item" active-color="green">About</a>
  <a href="#" class="nav-item" active-color="blue">Testimonials</a>
  <a href="#" class="nav-item" active-color="red">Blog</a>
  <a href="#" class="nav-item" active-color="rebeccapurple">Contact</a>
  <span class="nav-indicator"></span>
</nav>

        </div>
    )
}
