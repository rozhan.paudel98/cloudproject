import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./dashboard.component.css";
import { toast } from "react-toastify";
import axios from "axios";
import "./showproduct.css";

export class ShowProducts extends Component {
  constructor(props) {
    super();
    this.state = {
      products: [],

      name: "",
      brand: "",
      price: "",
      category: "",
      description: "",
      color: "",

      search: "",
      counter: 0,
      isDeleting: false,
    };
  }

  editItem = (id, i) => {
    const { name, brand, price, category, description, color } = this.state;
    let update = {
      name,
      brand,
      price,
      category,
      description,
      color,
    };
    axios.put(`http://localhost:6500/products/${id}`, update).then((data) => {
      this.props.history.push("/view-files");
      toast.success("Product Updated !");
    });
  };

  handleSearch = (event) => {
    this.setState({
      search: event.target.value,
    });
    console.log(this.state.search);
  };

  logoutfromapp = () => {
    localStorage.clear();
    toast.info("Successfully Signout ");
    this.props.history.push("/");
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  componentDidMount() {
    axios
      .get("http://localhost:6500/products/")
      .then((success) => {
        console.log(success.data[0].image);
        this.setState({
          products: success.data,
        });
      })
      .catch(function (error) {
        toast.warn(error);
      });
  }
  componentDidUpdate() {
    axios
      .get("http://localhost:6500/products/")
      .then((success) => {
        console.log(success.data[0].image);
        this.setState({
          products: success.data,
        });
      })
      .catch(function (error) {
        toast.warn(error);
      });
  }
  deleteProduct(id, index) {
    console.log(id);
    //show popup to confirm deletion
    const a = alert("Are you sure you want to delete?");

    this.setState({
      isDeleting: true,
    });
    axios
      .delete(`http://localhost:6500/products/${id}`, true)
      .then((item) => {
        toast.success(item.data.msg);
        const { products } = this.state;
        products.splice(index, 1);
        this.setState({
          products,
        });
      })
      .catch(function (error) {
        toast.error("Couldn't perfom task");
      })
      .finally(() => {
        this.setState({
          isDeleting: false,
        });
      });
  }

  render() {
    var deletebtn = this.state.isDeleting ? (
      <button disabled={true} className="btn btn-danger">
        Deleting Item
      </button>
    ) : (
      1
    );
    //Filtering the Products from search
    let filterdproducts = this.state.products.filter((product) => {
      return (
        product.name.toLowerCase().indexOf(this.state.search.toLowerCase()) !==
        -1
      );
    });

    return (
      <div className="mainD">
        <ul className="sidebar">
          <hr />
          <li className="insidelionefirst">
            <Link to="/dashboard">Dashboard</Link>
          </li>
          <hr />
          <li className="insidelione">
            <Link to="/view-files">Products</Link>
          </li>
          <hr />
          <li className="insidelione">
            <Link to="/add-item">Add item</Link>
          </li>
          <hr />
          <li className="insidelione">
            <Link to="/user-setting">User Setting</Link>
          </li>
          <hr />
          <li>
            {" "}
            <button
              className="btn btn-danger"
              onClick={this.logoutfromapp}
              id="logout"
            >
              Log Out
            </button>
          </li>
        </ul>

        <div className="content">
          <h1>Your lists of product</h1>
          <input
            type="text"
            className="form-control"
            placeholder="Search Item"
            name="searchfield"
            onChange={this.handleSearch}
          ></input>

          <div className="tobeflexed">
            {filterdproducts.map((products, i) => (
              <div className="card ml-5" style={{ width: "18rem" }}>
                <img
                  className="card-img-top"
                  src={`http://localhost:6500/files/${products.image}`}
                  alt="Card image cap"
                />
                <div className="card-body">
                  Name : {products.name} <br />
                  Brand : {products.brand} <br />
                  Color : {products.color} <br />
                  Price : {products.price} <br />
                  Description: {products.description}
                  <br />
                  <div className="row">
                    <div>
                      <button
                        type="button"
                        className="btn btn-primary"
                        data-toggle="modal"
                        data-target="#exampleModalLong"
                      >
                        Edit this item
                      </button>
                      {/* Modal */}
                      <div
                        className="modal fade"
                        id="exampleModalLong"
                        tabIndex={-1}
                        role="dialog"
                        aria-labelledby="exampleModalLongTitle"
                        aria-hidden="true"
                      >
                        <div className="modal-dialog" role="document">
                          <div className="modal-content">
                            <div className="modal-header">
                              <h5
                                className="modal-title"
                                id="exampleModalLongTitle"
                              >
                                Edit Product
                              </h5>
                              <button
                                type="button"
                                className="close"
                                data-dismiss="modal"
                                aria-label="Close"
                              >
                                <span aria-hidden="true">×</span>
                              </button>
                            </div>
                            <div className="modal-body">
                              <div className="formfield">
                                <form className="mx-5">
                                  <label>Name of the Product</label>
                                  <input
                                    className="form-control"
                                    placeholder={products.name}
                                    onChange={this.handleChange}
                                    type="text"
                                    name="name"
                                  />
                                  <label>Brand</label>
                                  <input
                                    className="form-control"
                                    onChange={this.handleChange}
                                    placeholder={products.brand}
                                    type="text"
                                    name="brand"
                                  />
                                  <label>Category</label>
                                  <input
                                    className="form-control "
                                    required={true}
                                    onChange={this.handleChange}
                                    placeholder={products.category}
                                    type="text"
                                    name="category"
                                  />

                                  <label>Price</label>
                                  <input
                                    type="text"
                                    required={true}
                                    onChange={this.handleChange}
                                    placeholder={products.price}
                                    className="form-control "
                                    name="price"
                                  />
                                  <label>Description</label>
                                  <input
                                    type="textbox"
                                    onChange={this.handleChange}
                                    placeholder={products.description}
                                    className="form-control "
                                    name="description"
                                  />

                                  <label>Color</label>
                                  <input
                                    type="text"
                                    onChange={this.handleChange}
                                    placeholder={products.color}
                                    className="form-control "
                                    name="color"
                                  />
                                </form>
                              </div>
                            </div>
                            <div className="modal-footer">
                              <button
                                type="button"
                                className="btn btn-secondary"
                                data-dismiss="modal"
                              >
                                Close
                              </button>
                              <button
                                type="button"
                                onClick={() => this.editItem(products._id, i)}
                                className="btn btn-primary"
                              >
                                Save
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <button
                      mt-2
                      onClick={() => this.deleteProduct(products._id, i)}
                      className="btn btn-danger pl-1"
                    >
                      Delete this Item
                    </button>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}
