import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../dashboard.component.css";
import { toast } from "react-toastify";
import axios from "axios";

export class UserSetting extends Component {
  constructor(props) {
    super();
    this.state = {
      email: "",
      oldpassword: "",
      newpassword: "",
    };
  }

  logoutfromapp = () => {
    localStorage.clear();
    toast.info("Successfully Signout ");
    this.props.history.push("/");
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  handleClick = (e) => {
    e.preventDefault();

    const data = {
      email: this.state.email,
      oldpassword: this.state.oldpassword,
      newpassword: this.state.newpassword,
    };
    console.log(data);
    axios
      .post("http://localhost:6500/users/modify-user-setting", data)
      .then((data) => {
        toast.info("Password Changed !");
        this.props.history.push("/view-files");
      })
      .catch((error) => {
        toast.info("Server Error Occured");
      });
  };

  render() {
    return (
      <div className="mainD">
        <ul className="sidebar">
          <hr></hr>
          <li className="insidelionefirst">
            <Link to="/dashboard">Dashboard</Link>
          </li>
          <hr></hr>
          <li className="insidelione">
            <Link to="/view-files">Products</Link>
          </li>
          <hr></hr>
          <li className="insidelione">
            <Link to="/add-item">Add item</Link>
          </li>
          <hr></hr>
          <li className="insidelione">
            <Link to="/user-setting">User Setting</Link>
          </li>
          <hr></hr>
          <li>
            {" "}
            <button
              className="btn btn-danger"
              onClick={this.logoutfromapp}
              id="logout"
            >
              Log Out
            </button>
          </li>
        </ul>

        <div className="content">
          <h1>Change or Modify User Setting </h1>
          <div className="formfield">
            <form className="mx-5" onSubmit={this.handleSubmit}>
              <label>Email</label>
              <input
                className="form-control less"
                placeholder="Enter your email"
                required={true}
                onChange={this.handleChange}
                type="text"
                name="email"
              />
              <label>Enter Old Password</label>
              <input
                className="form-control less "
                placeholder=" Enter your old password"
                onChange={this.handleChange}
                type="text"
                name="oldpassword"
              />
              <label>New password</label>
              <input
                className="form-control less"
                placeholder="Enter New Password"
                required={true}
                onChange={this.handleChange}
                type="text"
                name="newpassword"
              />

              <br></br>
              <button onClick={this.handleClick} className="btn btn-secondary">
                Confirm
              </button>
            </form>
          </div>
          <div className="photosection"></div>
        </div>
      </div>
    );
  }
}
