import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./dashboard.component.css";
import { toast } from "react-toastify";
import axios from "axios";

import { upload } from "../functions/fileupload";

export class AddProducts extends Component {
  constructor(props) {
    super();
    this.state = {
      name: null,
      brand: null,
      category: null,
      price: null,
      description: null,
      img: null,
    };
  }

  logoutfromapp = () => {
    localStorage.clear();
    toast.info("Successfully Signout ");
    this.props.history.push("/");
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleFile = (e) => {
    // this.state.img = e.target.files[0];
    this.setState({
      img: e.target.files[0],
    });
    console.log("consoled image file :", e.target.files[0]);
  };

  handleSubmit = (e) => {
    e.preventDefault();
    console.log("Image file is ", this.state.img);
    const { name, brand, category, price, description } = this.state;
    const dataSend = { name, brand, category, price, description };

    upload("POST", "http://localhost:6500/products", this.state.img, dataSend)
      .then((response) => {
        console.log(response);
        toast.info("Product added ");
        this.props.history.push("/view-files");
      })
      .catch((error) => {
        toast.warn("Failed to perform the operation");
      });
  };

  render() {
    return (
      <div className="mainD">
        <ul className="sidebar">
          <hr></hr>
          <li className="insidelionefirst">
            <Link to="/dashboard">Dashboard</Link>
          </li>
          <hr></hr>
          <li className="insidelione">
            <Link to="/view-files">Products</Link>
          </li>
          <hr></hr>
          <li className="insidelione">
            <Link to="/add-item">Add item</Link>
          </li>
          <hr></hr>
          <li className="insidelione">
            <Link to="/user-setting">User Setting</Link>
          </li>
          <hr></hr>
          <li>
            {" "}
            <button
              className="btn btn-danger"
              onClick={this.logoutfromapp}
              id="logout"
            >
              Log Out
            </button>
          </li>
        </ul>

        <div className="content">
          <h1>Add Product </h1>
          <div className="formfield">
            <form
              className="mx-5"
              onSubmit={this.handleSubmit}
              enctype="multipart/form-data"
            >
              <label>Name of the Product</label>
              <input
                className="form-control less"
                required={true}
                onChange={this.handleChange}
                type="text"
                name="name"
              />
              <label>Brand</label>
              <input
                className="form-control less "
                onChange={this.handleChange}
                type="text"
                name="brand"
              />
              <label>Category</label>
              <input
                className="form-control less"
                required={true}
                onChange={this.handleChange}
                type="text"
                name="category"
              />

              <label>Price</label>
              <input
                type="text"
                required={true}
                onChange={this.handleChange}
                className="form-control less"
                name="price"
              />
              <label>Description</label>
              <input
                type="textbox"
                onChange={this.handleChange}
                className="form-control wide less"
                name="description"
              />

              <label>Vendor</label>
              <input
                type="text"
                onChange={this.handleChange}
                className="form-control less"
                name="vendor"
              />
              <label>Color</label>
              <input
                type="text"
                onChange={this.handleChange}
                className="form-control less"
                name="color"
              />
              {/* <label>Discount</label>
            <input type="text" onChange={this.handleChange} className="form-control" name="discount" /> */}
              <label>Image of Product</label>
              <input
                type="file"
                required={true}
                onChange={this.handleFile}
                className="form-control less"
                name="img"
              />
              <br></br>
              <button type="submit" className="btn btn-secondary">
                Add Product
              </button>
            </form>
          </div>
          <div className="photosection"></div>
        </div>
      </div>
    );
  }
}
