import React from 'react';
import "./sidebar.css"

export default function SideBar() {
    return (
        <div>
           <nav>
  <ul>
    <li><span>Home</span></li>
    <li><span>Products</span></li>
    <li><span>Services</span></li>
    <li><span>Contact</span></li>
  </ul>
</nav>
        </div>
    )
}
