import React, { Component } from 'react'
import axios from 'axios';
import { toast } from 'react-toastify';


export default class ConfirmPassword extends Component {
    constructor(props){
        super(props);
        this.state={
            firstpassword: null,
            secondpassword: null,
            isSubmitting:false,
        }
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        this.setState({
            isSubmitting:true
        })
        setTimeout(()=>{
            this.setState({isSubmitting:false})
        },1500);
        const firstpassword= this.state.firstpassword;
        const secondpassword = this.state.secondpassword;
        if(firstpassword === secondpassword){
            let data={
                firstpassword,secondpassword
            }
            const url = "http://localhost:6500/users/reset-password";
            axios.post(url+this.props.match.params.id,data,{firstpassword})
                    .then((done)=>{
                        console.log("done is",done);

                    })
                    .catch((error)=>{
                        console.log("error is ",error);
                    })
        }
       
        
        
        
    }
    
     handleChange=(event)=>{
        this.setState({
            [event.target.name] : event.target.value
        })
    }


    render() {
        const btn = this.state.isSubmitting
        ? <button className="btn btn-danger">Resetting......</button>
        : <button className="btn btn-primary"  type="submit">Reset Password</button>
        return (
            <div className="container" id="forgot">
                <div className="form-gap"></div>
            <div className="row">
                <div className="col-md-4 col-md-offset-4">
                    <div className="panel panel-default">
                      <div className="panel-body">
                        <div className="text-center">
                          <h3><i className="fas fa-unlock-alt"></i></h3>
                          <h2 className="text-center">Forgot Password?</h2>
                          <p>You can reset your password here.</p>
                          <div className="panel-body">
                              <form className="form-group" onSubmit={this.handleSubmit}>
                            <label>Enter Password:</label>
                            <input type="password" onChange={this.handleChange} name="firstpassword" className="form-control" placeholder="Enter your New Password"></input>
                            <label>Confirm-Password</label>
                            <input type="password"  onChange={this.handleChange} name="secondpassword" className="form-control" placeholder="Confirm-Password"></input>
                            <br></br>
                            {btn}
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
        )
    }
}
