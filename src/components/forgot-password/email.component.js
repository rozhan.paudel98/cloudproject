import React, { Component } from "react";
import {Link} from "react-router-dom";
import "./email.component.css";
import axios from "axios";
import { toast } from "react-toastify";

export default class ForgotPasswordEmail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      isSubmitting: false,
    };
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({
      isSubmitting: true,
    });
    setTimeout(() => {
      this.setState({
        isSubmitting: false,
      });
    }, 1500);
    let email = this.state.email;
    axios
      .post("http://localhost:6500/users/forgot-password", { email: email })
      .then((success) => {
        console.log(success);
        toast.info(success.data.msg);
        this.props.history.push("/");
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  render() {
    const btn = this.state.isSubmitting ? (
      <button className="btn btn-danger">Submitting......</button>
    ) : (
      <button className="btn btn-primary" type="submit">
        Submit
      </button>
    );
    return (
      <div class="container" id="forgot">
        <div class="form-gap"></div>
        <div class="row">
          <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="text-center">
                  <h3>
                    <i class="fa fa-lock fa-4x"></i>
                  </h3>
                  <h2 class="text-center">Forgot Password?</h2>
                  <p>You can reset your password here.</p>
                  <div class="panel-body">
                    <form
                      id="register-form"
                      onSubmit={this.handleSubmit}
                      className="form-group"
                    >
                      <div class="form-group">
                        <div class="input-group">
                          <input
                            id="email"
                            name="email"
                            placeholder="email address"
                            onChange={this.handleChange}
                            className=" wego"
                            type="email"
                          />
                          <Link  to="/" type="button"> Go Back </Link>
                        </div>
                      </div>
                      <div class="form-group">{btn}</div>

                      <input
                        type="hidden"
                        class="hide"
                        name="token"
                        id="token"
                        value=""
                      />
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
