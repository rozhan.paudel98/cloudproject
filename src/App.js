import React from "react";
import { LoginForm } from "./components/login.component";
import TopNavbar from "./components/navbar/navbar.component";
import { RegiserForm } from "./components/register.component";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { NotFound } from "./components/pagenotfound.component";
import { Dashboard } from "./components/dashboard.component";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ForgotPasswordEmail from "./components/forgot-password/email.component";
import ConfirmPassword from "./components/forgot-password/retypepassword.component";
import { ShowProducts } from "./components/crudComponents/showproducts.components";
import { AddProducts } from "./components/crudComponents/addproduct.component";
import { UserSetting } from "./components/crudComponents/UserSetting/usersetting";

const ProctectedRoute = ({ component: Component, ...props }) => {
  return (
    <Route
      {...props}
      render={
        (props) =>
          localStorage.getItem("token") ? (
            <>
              <Component {...props}></Component>
            </>
          ) : (
            <Redirect to="/"></Redirect>
          ) // TODO send props
      }
    />
  );
};

export function App() {
  return (
    <div>
      <ToastContainer />
      <Router>
        {/* <TopNavbar isLoggedIn={localStorage.getItem("token") ? true : false} /> */}
        <Switch>
          <Route path="/" exact component={LoginForm} />
          <Route path="/login" component={LoginForm} />
          <Route path="/register" component={RegiserForm} />
          <ProctectedRoute path="/dashboard" component={Dashboard} />
          {/* <Route path="/dashboard" component={Dashboard}/> */}
          <Route path="/forgot-password" component={ForgotPasswordEmail} />
          <ProctectedRoute path="/view-files" component={ShowProducts} />
          <ProctectedRoute path="/add-item" component={AddProducts} />
          <ProctectedRoute path="/user-setting" component={UserSetting} />
          <Route path="/reset-password/:id" component={ConfirmPassword} />

          <Route component={NotFound} />
        </Switch>
      </Router>
    </div>
  );
}
